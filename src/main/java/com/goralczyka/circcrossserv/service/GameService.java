package com.goralczyka.circcrossserv.service;

import com.goralczyka.circcrossserv.model.Board;
import com.goralczyka.circcrossserv.model.GameRoom;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by adam on 19.05.17.
 */
@Log4j
@Component
public class GameService {

    public CleanerService cleanerService = new CleanerService(this);

    // Token Gry, Pokój
    public Map<String, GameRoom> openGameRooms = new HashMap<>();
    public Map<String, GameRoom> closedGameRooms = new HashMap<>();

    /**
     * Utworzenie gry.
     * @return token gracza.
     */
    public String createGame(final String gameToken) {
        cleanerService.cleanOldGames();
        if (isPossibleToCreateGame(gameToken)) {
            String userToken = UUID.randomUUID().toString();
            openGameRooms.put(gameToken, new GameRoom(userToken));
            log.info(gameToken + " | Utworzono grę przez gracza " + userToken);
            return userToken;
        }
        else {
            //Exception?
            return null;
        }
    }

    private Boolean isPossibleToCreateGame(final String gameToken) {
        if (openGameRooms.containsKey(gameToken) || closedGameRooms.containsKey(gameToken)) {
            log.info(gameToken + " | Istnieje gra o wskazanym tokenie");
            return false;
        }
        return true;
    }

    private Boolean isPossibleToJoinToGame(final String gameToken) {
        return openGameRooms.containsKey(gameToken);
    }

    /**
     * Sprawdzenie czy drugi gracz dołączył do gry.
     * @param gameToken token gry
     * @return czy drugi gracz dołączył do gry
     */
    public Boolean isSecondPlayer(final String gameToken) {
        if (closedGameRooms.containsKey(gameToken)) {
            log.info(gameToken + " | W pokoju jest komplet graczy");
            return true;
        }
        else if (openGameRooms.containsKey(gameToken)) {
            log.info(gameToken + " | W pokoju jest ciągle jeden gracz");
            return false;
        }
        log.info(gameToken + " | Wskazany pokój nie istnieje");
        return false;
    }

    /**
     * Dołączenie do gry
     * @param gameToken token gry
     * @return token użytkownika
     */
    public String joinGame(final String gameToken) {
        if (isPossibleToJoinToGame(gameToken)) {
            String userToken = UUID.randomUUID().toString();
            setSecondUserForGame(gameToken, userToken);
            moveGameToClosedRooms(gameToken);
            log.info(gameToken + " | Gracz " + userToken + " dołączył do pokoju. Pokój zostaje zamknięty.");
            return userToken;
        }
        // EXCEPT
        return null;
    }

    public Boolean isPlayerTurn(final String gameToken, final String userToken) {
        log.info(gameToken + " | Gracz " + userToken + " sprawdza czyj ruch jest");
        log.info(gameToken + " | Now is " + closedGameRooms.get(gameToken).whosePlayerIsTurn() + " turn.");
        return closedGameRooms.get(gameToken).whosePlayerIsTurn().equals(userToken);
    }

    /**
     * Sprawdzanie czy gra jest skonczona i w razie ukonczenia, usuwanie gry z pamieci
     * @param gameToken token gry
     * @return 0 - gra trwa, 1 - gra skonczona, przegrana; 2 - gra skonczona - remis
     */
    public Integer isGameEnded(final String gameToken) {
            GameRoom room = closedGameRooms.get(gameToken);
        if (room.isGameEnded()) {
            // przewiduje ze to zapytanie sypnie gracz przegrany, bo wygrany dostanie wynik wczensniej
            // dlatego pozwole sobie usunac juz ta gre
            closedGameRooms.remove(gameToken);
            if (room.isDraw()) {
                return 2;
            } else {
                return 1;
            }
        }
        return 0;
    }

    /**
     * Wykonywanie ruchu
     * @param gameToken token gry
     * @param userToken token uzytkownika
     * @param x - os x
     * @param y - os y
     * @return -1 - blad, 0 - ruch wykonany gra w toku, 1 - ruch wykonany i zwyciestwo, 2 - ruch wykonany i remis
     */
    public Integer turn(final String gameToken, final String userToken, final Integer x, final Integer y) {
        if (isPlayerTurn(gameToken, userToken)) {
            log.info(gameToken + " | Gracz " + userToken + " zaznacza x=" + x + " y= " + y);
            return closedGameRooms.get(gameToken).turn(userToken, x, y);
        }
        return -1;
    }

    public HashMap<String, String> getBoard(final String gameToken) {
        HashMap<String, String> map = new HashMap<>();
        Board board = closedGameRooms.get(gameToken).getBoard();
        for (Integer i = 0; i < board.BOARD_SIZE; i++) {
            for (Integer j = 0; j < board.BOARD_SIZE; j++) {
                map.put("p" + i.toString() + j.toString(), parseStateToSymbol(board.getBoardState(i, j)));
            }
        }
        return map;
    }

    private String parseStateToSymbol(Board.State state) {
        if (state == Board.State.BLANK) {
            return "";
        } else if (state == Board.State.X) {
            return "X";
        } else if (state == Board.State.O) {
            return "O";
        }
        return "";
    }

    private void moveGameToClosedRooms(final String gameToken) {
        closedGameRooms.put(gameToken, openGameRooms.remove(gameToken));
    }

    private void setSecondUserForGame(final String gameToken, final String userToken) {
        openGameRooms.get(gameToken).setSecondPlayerToken(userToken);
    }
}
