package com.goralczyka.circcrossserv.service;

/**
 * Created by adam on 21.05.17.
 */
public class CleanerService {

    private static final Long TIME_TO_INACTIVE = 1000L * 60 * 5; // 5 minut

    private GameService gameService;

    public CleanerService(final GameService gameService) {
        this.gameService = gameService;
    }

    public void cleanOldGames() {
        // tymczasowo wykomentowuje bo w sumie wcale tego nie chce
/*        for (String key : gameService.openGameRooms.keySet()) {
            if (gameService.openGameRooms.get(key).isOlderThan(TIME_TO_INACTIVE)) {
                gameService.openGameRooms.remove(key);
            }
        }*/
    }
}
