package com.goralczyka.circcrossserv.exceptions;

/**
 * Created by adam on 20.05.17.
 */
public class CantSetMarkerException extends Exception {
    public CantSetMarkerException(String message) {
        super(message);
    }
}
