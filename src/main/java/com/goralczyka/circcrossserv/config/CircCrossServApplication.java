package com.goralczyka.circcrossserv.config;

import com.goralczyka.circcrossserv.service.GameService;
import lombok.extern.log4j.Log4j;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.common.TypeFactoryImpl;
import org.apache.xmlrpc.metadata.XmlRpcSystemImpl;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcErrorLogger;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.XmlRpcServletServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.HttpRequestHandler;

@SpringBootApplication
@Log4j
public class CircCrossServApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircCrossServApplication.class, args);
	}

	@Bean(name = "/xmlrpc")
	public HttpRequestHandler requestHandler() throws XmlRpcException {
		XmlRpcServerConfigImpl config = new XmlRpcServerConfigImpl();
		config.setEnabledForExtensions(true);
		config.setContentLengthOptional(false);

		// https://ws.apache.org/xmlrpc/handlerCreation.html
		PropertyHandlerMapping propertyHandlerMapping = new PropertyHandlerMapping();
		GameService gameService = new GameService();
        propertyHandlerMapping.setRequestProcessorFactoryFactory(new GameServiceProcessorFactoryFactory(gameService));
        propertyHandlerMapping.setVoidMethodEnabled(true);
        propertyHandlerMapping.addHandler("GameService", GameService.class);

		XmlRpcSystemImpl.addSystemHandler(propertyHandlerMapping);

		XmlRpcServletServer server = new XmlRpcServletServer();
		server.setConfig(config);
		server.setErrorLogger(new XmlRpcErrorLogger());
		server.setHandlerMapping(propertyHandlerMapping);
		server.setTypeFactory(new TypeFactoryImpl(server));

		log.info("xml-rpc endpoint created.");
		return server::execute;
	}
}
