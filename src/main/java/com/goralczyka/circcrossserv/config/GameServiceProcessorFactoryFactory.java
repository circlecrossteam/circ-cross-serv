package com.goralczyka.circcrossserv.config;

import com.goralczyka.circcrossserv.service.GameService;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.server.RequestProcessorFactoryFactory;

/**
 * https://ws.apache.org/xmlrpc/handlerCreation.html
 * Created by adam on 20.05.17.
 */
public class GameServiceProcessorFactoryFactory implements RequestProcessorFactoryFactory {

    private final RequestProcessorFactory factory = new GameServiceProcessorFactory();
    private final GameService gameService;

    public GameServiceProcessorFactoryFactory(GameService gameService) {
        this.gameService = gameService;
    }

    @Override
    public RequestProcessorFactory getRequestProcessorFactory(Class aClass) throws XmlRpcException {
        return factory;
    }

    private class GameServiceProcessorFactory implements RequestProcessorFactory {
        @Override
        public Object getRequestProcessor(XmlRpcRequest xmlRpcRequest) throws XmlRpcException {
            return gameService;
        }
    }
}
