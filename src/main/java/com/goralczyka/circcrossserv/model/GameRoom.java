package com.goralczyka.circcrossserv.model;

import com.goralczyka.circcrossserv.exceptions.CantSetMarkerException;
import lombok.Data;

import java.util.Date;

/**
 * Created by adam on 20.05.17.
 */
@Data
public class GameRoom {

    enum Turn {
        FIRST,
        SECOND
    }

    private String firstPlayerToken;
    private String secondPlayerToken;
    private Board board;
    private Date lastRequestForGame;
    private Turn turn;

    public GameRoom(final String firstPlayerToken) {
        this.firstPlayerToken = firstPlayerToken;
        this.secondPlayerToken = null;
        this.board = new Board();
        this.lastRequestForGame = new Date();
        this.turn = Turn.FIRST;
    }

    public void updateRequestDate() {
        lastRequestForGame = new Date();
    }

    public Integer turn(final String userToken, Integer x, Integer y) {
        updateRequestDate();
        if (turn == Turn.FIRST && firstPlayerToken.equals(userToken) ||
                turn == Turn.SECOND && secondPlayerToken.equals(userToken)) {
            Integer score;
            try {
                score = parseScoreToStatus(board.turn(x, y));
            } catch (CantSetMarkerException e) {
                return -1;
            }
            changeTurn();
            return score;
        }
        return -1;
    }

    private Integer parseScoreToStatus(Board.Score score) {
        if (Board.Score.WIN.equals(score)) {
            return 1;
        } else if (Board.Score.IN_PROGRESS.equals(score)) {
            return 0;
        } else if (Board.Score.DRAW.equals(score)) {
            return 2;
        } else {
            return -1;
        }
    }

    private void changeTurn() {
        if (this.turn == Turn.FIRST)
            this.turn = Turn.SECOND;
        else
            this.turn = Turn.FIRST;
    }

    /**
     * Zwraca token gracza którego jest ruch.
     * @return token gracza którego jest ruch
     */
    public String whosePlayerIsTurn() {
        if (turn == Turn.FIRST) {
            return firstPlayerToken;
        } else if (turn == Turn.SECOND) {
            return secondPlayerToken;
        }
        return null;
    }

    /**
     * @return Czy gra jest zakończona
     */
    public Boolean isGameEnded() {
        return board.getEnded();
    }

    public Boolean isDraw() {
        return board.getIsDraw();
    }

    public Boolean isOlderThan(Long time) {
        return new Date().getTime() - lastRequestForGame.getTime() > time;
    }
}
