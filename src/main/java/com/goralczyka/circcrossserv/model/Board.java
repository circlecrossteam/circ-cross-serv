package com.goralczyka.circcrossserv.model;

import com.goralczyka.circcrossserv.exceptions.CantSetMarkerException;
import lombok.Getter;

/**
 * Klasa tablicy do TicTacToe.
 * Markery:
 * 0 - pusta kratka
 * 1 - Kółko
 * 2 - Krzyżyk
 * Created by adam on 19.05.17.
 */
public class Board {

    public static final Integer BOARD_SIZE = 3;

    public enum State {
        BLANK,
        X,
        O
    }

    public enum Score {
        IN_PROGRESS,
        WIN,
        LOSE,
        DRAW
    }

    private State[][] board;
    private State currentPlayerMark;
    private Integer moveCount;
    @Getter
    private Boolean ended;
    @Getter
    private Boolean isDraw;

    public Board() {
        this.board = new State[BOARD_SIZE][BOARD_SIZE];
        this.currentPlayerMark = State.X;
        this.moveCount = 0;
        this.ended = false;
        this.isDraw = false;
        initializeBoard();
    }

    /**
     * Inicjalizacja tablicy pustymi elementami.
     */
    private void initializeBoard() {
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                board[i][j] = State.BLANK;
            }
        }
    }

    /**
     * Ruch gracza. Ustawia marker w odpowiednim miejscu i sprawdza stan gry (wygrana, koniec, czy trwa)
     * @param x oś x
     * @param y oś y
     * @return stan gry (wygrana, koniec, czy trwa)
     */
    public Score turn(final Integer x, final Integer y) throws CantSetMarkerException {
        State playerMarker = currentPlayerMark;
        if (setMarker(x, y)) {
            moveCount++;
        }

        Score score = checkWinner(playerMarker, x, y);
        if (score == Score.DRAW || score == Score.WIN || score == Score.LOSE) {
            this.ended = true;
        }

        return score;
    }

    /**
     * Ustawia marker we wskazanych współrzędnych
     * @param x oś x numerowana od 0
     * @param y oś y numerowana od 0
     * @return czy udało się ustawić marker
     */
    private Boolean setMarker(final Integer x, final Integer y) throws CantSetMarkerException {
        if (board[x][y] == State.BLANK) {
            board[x][y] = currentPlayerMark;
            changeMarker();
            return true;
        }
        throw new CantSetMarkerException("Cant set marker in postiontion + " + x + ", " + y);
    }

    /**
     * Zmienia marker gracza na przeciwny.
     */
    private void changeMarker() {
        if (currentPlayerMark == State.X) {
            currentPlayerMark = State.O;
        }
        else if (currentPlayerMark == State.O) {
            currentPlayerMark = State.X;
        }
    }


    /**
     * Metoda zwraca stan gry po ruchu gracza.
     * @param mark symbol gracza
     * @param x oś x
     * @param y oś y
     * @return stan gry
     */
    private Score checkWinner(final State mark, final Integer x, final Integer y) {

        // Sprawdzenie kolumn
        for (int i = 0; i < BOARD_SIZE; i++) {
            if (board[x][i] != mark)
                break;
            if (i == BOARD_SIZE - 1){
                return Score.WIN;
            }
        }

        // Sprawdzenie wierszy
        for (int i = 0; i < BOARD_SIZE; i++){
            if (board[i][y] != mark)
                break;
            if (i == BOARD_SIZE - 1){
                return Score.WIN;
            }
        }

        // Sprawdzenie przekątnych
        if (x == y){
            for (int i = 0; i < BOARD_SIZE; i++){
                if (board[i][i] != mark)
                    break;
                if (i == BOARD_SIZE - 1){
                    return Score.WIN;
                }
            }
        }

        if (x + y == BOARD_SIZE - 1){
            for(int i = 0; i < BOARD_SIZE; i++){
                if(board[i][(BOARD_SIZE - 1) - i] != mark)
                    break;
                if(i == BOARD_SIZE - 1){
                    return Score.WIN;
                }
            }
        }

        // Sprawdzenie remisu

        if (moveCount == (Math.pow(BOARD_SIZE, 2))){
            isDraw = true;
            return Score.DRAW;
        }

        return Score.IN_PROGRESS;
    }

    public State getBoardState(int x, int y) {
        return board[x][y];
    }
}
